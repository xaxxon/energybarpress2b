// stepper DRV8825 pololu  https://www.pololu.com/product/2133


/* commands 

u - change direction to up 
d - change direction to down
e - energize stepper test (for checking stepper current draw)
0 - stop, disable
1 - move 0.1 mm
2 - move 0.5 mm
3 - move 1 mm 
5 - move 10 mm
7 - move down continuous
8 - move up continuous
U - move up continuous with auto load target holding active
h - home (move fully down to limit switch, then up to HOMEPOSITION)
P + BYTE + BYTE - goto position micrometers [0-65535]
t - tare (zero load cell)
g + BYTE + BYTE - load target, grams [0-65535]
j + BYTE - temp  setpoint TOP, deg C [0-255]
J + BYTE - temp  setpoint BOTTOM, deg C [0-255]
p - top heaters on 
o - top heaters off
l - bottom heaters on
k - bottom heaters off
m - convect fan on 
n - convect fan off
b - case fan on
c - case fan off
x - everything off
i - output both internal temps
A - auto temp control start TOP
C - auto temp control start BOTTOM
B - auto temp control stop, heaters off TOP
R - auto temp control stop, heaters off BOTTOM
v - toggle verbose output
D - disable output
E - enable output
S - short output once
F - position + force output once
X - reserved for host heartbeat ping
y - version

*/

#include "HX711.h"
#include "Adafruit_MAX31855.h"

// stepper pins
#define STEPPIN		  8
#define DIRPIN		  4
#define SLEEPPIN      10

// limit switch
#define SWITCH		  12
#define LED 		  13

// load cell 
#define DOUT  		  11
#define CLK  		  2

// relays
#define TOPHEATERS    A0
#define BOTTOMHEATERS   A1
#define CONVECTFAN    A2
#define CASEFAN       9

// thermocouple bar
#define DOTOP		  A3
#define CSTOP		  A4
#define CLKTOP		  A5

// thermocouple oven
#define DOBOTTOM	  5
#define CSBOTTOM	  6
#define CLKBOTTOM	  7

// spare pins: 3


Adafruit_MAX31855 thermocoupleTop(CLKTOP, CSTOP, DOTOP);
Adafruit_MAX31855 thermocoupleBottom(CLKBOTTOM, CSBOTTOM, DOBOTTOM);

#define SECOND		  1000000

// load cell
HX711 scale(DOUT, CLK);
const double CALIBRATION_FACTOR = -9430.0; // obtain using the HX711_Calibration sketch (for Kg)
unsigned long nextLoadCheck = 0;
const unsigned long LOADCHECKINTERVAL = 1000; // microseconds (note: main loop probably > than 1000ms)
double currentload = 0;
const double LOADMAX = 50.0; // Kg - absolute maximum force, both directions
const double SAFELOAD = 10.0; // Kg - when position unknown or near slide out tray
double targetLoad = 15.0; // Kg - arbitrary, can be user set
const double LOADTOLERANCE = 2.0; // Kg
boolean autoloadhold = false;
uint8_t loadbuffer[500]; // testing load cell buffer mem available

// command byte buffer 
int buffer[16];
int commandSize = 0;

// stepper motor
const unsigned long SPEEDDELAYNORMAL = 1000; // microseconds, smaller=faster
const unsigned long SPEEDDELAYSLOW = 20000; // microseconds, smaller=faster -- used when moving UP above SAFELOAD
unsigned long speeddelay = SPEEDDELAYNORMAL;
unsigned long laststeptime = 0;
int goalsteps = 0;
int stepnum = 0;
byte stepmode = HIGH;
boolean stepperenabled = false;

// position tracking, mm
#define DIRECTIONUP      1
#define DIRECTIONDOWN    0
#define POSITIONUNKNOWN  -1.0
const double MMPERFULLSTEP = 0.01; // fixed, derived from step angle, screw pitch
double position = POSITIONUNKNOWN; // negative = unknown
int direction = DIRECTIONDOWN;
const double MAXPOSITION = 55.0; // abolute max stroke before damage occurs
boolean homing = false;
const double HOMEPOSITION = 3.0;
const double SAFEPOSTHRESHOLD = 25.0; // SAFELOAD applies below this position
double targetpos = 0.0;

// limit switch
int limitswitch = 0;
unsigned long nextlimitswitch = 0;

// heaters, fans
boolean topheaterson = false;
boolean bottomheaterson = false;
boolean convectfanon = false;
boolean casefanon = false;
boolean casefanstayoff = false;
#define TURNON    1
#define TURNOFF   0

// temps
const double MAXTEMP = 200.0; // C
const double MAXINTERNALTEMP = 65.0;  
double toptemp = -99.0;
double bottomtemp = -99.0;
double internaltemp = -99.0;
const double CASEFANTHRESHOLD = 30.0; // C
const unsigned long TEMPCHECKINTERVAL = 2*SECOND; // microseconds
unsigned long nextBottomTempCheck = 0;
unsigned long nextTopTempCheck = 0;
unsigned long nextInternalTempCheck = 0;
const unsigned long MAXTEMPCHECKINTERVAL = 10*SECOND; // microseconds
int toptempnan = 0;
int bottomtempnan = 0;
const int MAXTEMPNAN = 5;

// temp setpoint control Top
double setPointTOP = 150.0; // dec C
const long DUTYWINDOW_TOP = 10*SECOND; 
const double DUTYPERCENT_TOP = 0.6; 
unsigned long windowStartTimeTOP;
bool tempControlActiveTOP = false;
double lastTempTOP = 0;
double lastTempDiffTOP = 0;

const double AUTOALWAYSONDIFF = 40.0; // C -- will heat continuous to setpoint minus this

// temp setpoint control Bottom
double setPointBOTTOM = 150.0; // dec C
const long DUTYWINDOW_BOTTOM = 10*SECOND; 
const double DUTYPERCENT_BOTTOM = 1.0; 
unsigned long windowStartTimeBOTTOM;
bool tempControlActiveBOTTOM = false;
double lastTempBOTTOM = 0;
double lastTempDiffBOTTOM = 0;

// printing output
boolean enableoutput = true;
boolean verbose = false; // set to false for use with java GUI
const unsigned long OUTPUTINTERVAL = 1*SECOND;
unsigned long nextOutput = 0;
const unsigned long FORCEOUTPUTINTERVAL = 0.1*SECOND;
unsigned long nextForceOutput = 0;

// host heartbeat safety
unsigned long lastHostHeartBeat = 0;
const unsigned long HOST_HEARTBEAT_LOST = 20*SECOND; // all off 

// clock
unsigned long time = 0;  // microseconds
const unsigned long  HANDSHAKEDELAY = 3*SECOND; 
unsigned long startup = 0;
 
 
void setup() {
	Serial.begin(115200);    
	Serial.println("<reset>");
	version();

	/* stepper driver */
	pinMode(STEPPIN,OUTPUT); 
	pinMode(DIRPIN,OUTPUT);
	pinMode(SLEEPPIN, OUTPUT);
	digitalWrite(SLEEPPIN, LOW);

	if (verbose) Serial.print("mode: ");
	down();
	
	/* limit switch */
	pinMode(SWITCH, INPUT); 
	pinMode(LED, OUTPUT); 
	
	/* load cell */
	scale.set_scale(CALIBRATION_FACTOR); 
	scale.tare(); // zero
	
	/* relays */
	pinMode(TOPHEATERS, OUTPUT); 
	digitalWrite(TOPHEATERS, HIGH); // OFF
	pinMode(BOTTOMHEATERS, OUTPUT); 
	digitalWrite(BOTTOMHEATERS, HIGH); // OFF
	pinMode(CONVECTFAN, OUTPUT); 
	digitalWrite(CONVECTFAN, HIGH); // OFF
	pinMode(CASEFAN, OUTPUT); 
	digitalWrite(CASEFAN, HIGH); // OFF
	
	startup = micros() + HANDSHAKEDELAY;
	
	// space out temp readings, reduce NaNs?
	nextBottomTempCheck = startup;
	nextTopTempCheck = startup + 1*SECOND;
	nextInternalTempCheck = startup + 2*SECOND;
	
	memset(loadbuffer,0x00,sizeof(loadbuffer));
}

void loop() {
	
	time = micros();
	
	if( Serial.available() > 0) manageCommand(); 
	
	if (time < startup) return; // wait for initial HANDSHAKEDELAY with devices
	
	if (stepperenabled && time >= laststeptime + speeddelay)  steppermove();
	
	if (time >= nextLoadCheck) loadread();

	if (time >= nextlimitswitch) readlimitswitch();

	if (time >= nextTopTempCheck) {
		readTopTemp();
		if (tempControlActiveTOP) tempControlTop(); 
	}
	
	if (time >= nextBottomTempCheck) {
		readBottomTemp();
		if (tempControlActiveBOTTOM) tempControlBottom(); 
	}
	
	if (time >= nextInternalTempCheck) readInternalTemp();

	if (time >= nextOutput && !stepperenabled)  {
		if (verbose) printOutput();
		else if (enableoutput) printShortOutput();
	}
	
	if (time >= nextForceOutput && enableoutput)  printForceOutput();
	
	if (time > lastHostHeartBeat + HOST_HEARTBEAT_LOST  && lastHostHeartBeat !=0 && !verbose) {
		lastHostHeartBeat = 0;
		allOff();
		Serial.println("<host lost>");
	} 
	
}

/* handle incoming serial input */
void manageCommand() {

	int input = Serial.read();
	buffer[commandSize++] = input;
	
	if((input == 13) || (input == 10) && commandSize > 0 && buffer[0] != 'g' && buffer[0] != 'P') {
		commandSize--; // ignore newline
		parseCommand();
		commandSize = 0; 
	} 
	else if ((buffer[0]=='g' || buffer[0] == 'P') && commandSize == 3) { // commands with DWORD values
		parseCommand();
		commandSize = 0;
	}
	else if ((input == 13 || input == 10) && commandSize == 1)  commandSize = 0; // ignore solitary newlines

    lastHostHeartBeat = time;
	
}

/* serial input command tree */
void parseCommand(){

	if (buffer[0] == 'u') up(); 
					// direction up
	else if (buffer[0] == 'd') down();			// direction down
	
	else if (buffer[0] == 'e') enableTest(); 
	
	else if (buffer[0] == '0') {
		autoloadhold = false;
		disableMotor(); 
	}
	
	else if (buffer[0] == '1') go(0.1);  
	
	else if (buffer[0] == '2') go(0.5);  
	
	else if (buffer[0] == '3') go(1.0); 
	
	else if (buffer[0] == '5') go(10.0); 
	
	else if (buffer[0] == '7') {
		autoloadhold = false;
		goDownContinuous();
	}
	
	else if (buffer[0] == '8') {
		autoloadhold = false;
		goUpContinouous();
	}
	
	else if (buffer[0] == 'U') {
		autoloadhold = true;
		goUpContinouous();
	}
	
	else if (buffer[0] == 'h' and limitswitch != 1) {   // home
		autoloadhold = false;
		position = POSITIONUNKNOWN;
		if (direction != DIRECTIONDOWN) down();
		go(99.0);
		homing = true;
	}
	
	else if (buffer[0] == 'P') {
		int a = buffer[1];
		int b = buffer[2]; 
		double tp = ((long) word(b,a));
		tp /= 1000; // convert to mm
		if (tp > MAXPOSITION) targetpos = MAXPOSITION;
		else if (tp < 0) tp = 0;
		gotoposition(tp);
	}
	
	else if (buffer[0] == 't') scale.tare();
	
	else if (buffer[0] == 'g') {
		int a = buffer[1];
		int b = buffer[2]; 
		targetLoad = (long) word(b,a);
		targetLoad /= 1000; // convert to kg
		if (targetLoad > LOADMAX) targetLoad = LOADMAX;
	}
	
	else if (buffer[0] == 'y') version();
	
	else if (buffer[0] == 'p') topheaters(TURNON);
	
	else if (buffer[0] == 'o')  topheaters(TURNOFF);
	
	else if (buffer[0] == 'l') bottomheaters(TURNON);

	else if (buffer[0] == 'k') bottomheaters(TURNOFF);
	
	else if (buffer[0] == 'm') convectfan(TURNON);

	else if (buffer[0] == 'n') convectfan(TURNOFF);
	
	else if (buffer[0] == 'b') {
		casefan(TURNON);
		casefanstayoff = false;
	}

	else if (buffer[0] == 'c') {
		casefan(TURNOFF);
		casefanstayoff = true;
	}
	
	else if (buffer[0] == 'j') {
		setPointTOP = (double) buffer[1];
		if (setPointTOP > MAXTEMP) setPointTOP = MAXTEMP;
	}
	
	else if (buffer[0] == 'J') {
		setPointBOTTOM = (double) buffer[1];
		if (setPointBOTTOM > MAXTEMP) setPointBOTTOM = MAXTEMP;
	}
	
	else if (buffer[0] == 'A') tempControlStartTOP();
	
	else if (buffer[0] == 'C') tempControlStartBOTTOM();

	else if (buffer[0] == 'B') topTempControlStop();
	
	else if (buffer[0] == 'R') bottomTempControlStop();
	
	else if (buffer[0] == 'x') allOff();
	
	else if (buffer[0] == 'v') toggleVerbose();
	
	else if (buffer[0] == 'i') {
		Serial.print("internal bar: ");
		Serial.print(thermocoupleTop.readInternal());
		Serial.print("C, oven: ");
		Serial.print(thermocoupleBottom.readInternal());
		Serial.println("C");
	}
	
	else if (buffer[0] == 'D') enableoutput = false;
	else if (buffer[0] == 'E') enableoutput = true;
	else if (buffer[0] == 'S') printShortOutput();
	else if (buffer[0] == 'F') printForceOutput();
		
}

void topheaters(boolean state) {
	if (state == TURNON && bottomtemp < MAXTEMP) {
		digitalWrite(TOPHEATERS, LOW);
		if (verbose) Serial.println("top heaters ON");
		topheaterson = true;
	}
	else if (state == TURNOFF) {
		digitalWrite(TOPHEATERS, HIGH);
		if (verbose) Serial.println("top heaters OFF");
		topheaterson = false;
	}
}

void bottomheaters(boolean state) {
	if (state == TURNON && bottomtemp < MAXTEMP) {
		digitalWrite(BOTTOMHEATERS, LOW);
		if (verbose) Serial.println("bottom heaters ON");
		bottomheaterson = true;
	}
	else if (state == TURNOFF) {
		digitalWrite(BOTTOMHEATERS, HIGH);
		if (verbose) Serial.println("bottom heaters OFF");
		bottomheaterson = false;
	}
}

void convectfan(boolean state) {
	if (state == TURNON) {
		digitalWrite(CONVECTFAN, LOW);
		convectfanon = true;
		if (verbose) Serial.println("convect fan ON");
	}
	else  {
		digitalWrite(CONVECTFAN, HIGH);
		convectfanon = false;
		if (verbose) Serial.println("convect fan OFF");
	}
}

void casefan(boolean state) {
	if (state == TURNON) {
		digitalWrite(CASEFAN, LOW);
		casefanon = true;
		if (verbose) Serial.println("case fan ON");
	}
	else  {
		digitalWrite(CASEFAN, HIGH);
		casefanon = false;
		if (verbose) Serial.println("case fan OFF");
	}
}

/* stepper movement */
void steppermove() {
	if (stepnum >= goalsteps) { // position reached, stop
		boolean h = homing;
		disableMotor();
		if (h) { // just homed, zero scale after delay
			delay(500);
			scale.tare(); 
		}
	}
	else { // do step
		laststeptime = time;
		if (stepmode == LOW) { 
			stepnum ++;
			stepmode = HIGH;
			
			if (position != POSITIONUNKNOWN) { // track position
				if (direction == DIRECTIONUP) position += MMPERFULLSTEP;
				else position -= MMPERFULLSTEP;
				if (position >= MAXPOSITION) {
					disableMotor();
					if (verbose) Serial.println("upper limit reached");
					down();
				}
			}
		}
		else { stepmode = LOW; }
		
		digitalWrite(STEPPIN, stepmode); 
	}
}

/* change direction to up */
void up()  {
	if (verbose) Serial.println("up");
	digitalWrite(DIRPIN, LOW); 
	direction = DIRECTIONUP;
}

/* change direction to down */
void down() {
	if (verbose) Serial.println("down");
	digitalWrite(DIRPIN, HIGH); 
	direction = DIRECTIONDOWN;
}

/* enable motor and set relative position target */
void go(double mm) {
	
	if (limitswitch == 1 and direction == DIRECTIONDOWN) {
		if (verbose) Serial.println("lower limit reached");
		return;
	}
	
	if (position >= MAXPOSITION and direction == DIRECTIONUP) {
		if (verbose) Serial.println("upper limit reached");
		return;
	}
	
	goalsteps = (int) (mm/MMPERFULLSTEP);
	stepnum = 0;
	stepmode = HIGH;
	enableMotor();
	digitalWrite(STEPPIN, stepmode);
}

void goDownContinuous() {
	if (direction != DIRECTIONDOWN) down();
	if (position != POSITIONUNKNOWN) go(position-HOMEPOSITION); // if position known, stop at home pos
	else go(99.0);
}

void goUpContinouous() {
	if (direction != DIRECTIONUP) up();
	go(99.0);
}

void gotoposition(double p) {
	targetpos = p;
	if (position == POSITIONUNKNOWN) return;
	
	if (targetpos > position) {
		up();
		go(targetpos - position);
	}
	else if (targetpos < position) {
		down();
		go(position - targetpos);
	}
}

/* energize motor */
void enableMotor() {
	digitalWrite(SLEEPPIN, HIGH);
	stepperenabled = true;
	if (verbose) Serial.println("stepper enable");
	//else printShortOutput(); // update java 1st, all serial output disabled when stepper moving to prevent jerky movement
}

/* stop movement, de-energize motor */
void disableMotor() {
	digitalWrite(SLEEPPIN, LOW);
	if (verbose) Serial.println("stepper disable");
	goalsteps = 0;
	stepperenabled = false;
	homing = false;
}

/* test energize motor */
void enableTest() {
	if (verbose) Serial.println("enable test");
	digitalWrite(SLEEPPIN, HIGH);
}

void allOff() {
	disableMotor();
	topheaters(TURNOFF);
	bottomheaters(TURNOFF);
	topTempControlStop();
	bottomTempControlStop();
	convectfan(TURNOFF);
	autoloadhold = false;
	if (verbose) Serial.println("all OFF");
}

/* check if limit switch on or off */
void readlimitswitch() {
	
	if( (digitalRead(SWITCH) == LOW) && (limitswitch == 1) ) { // off
		if (verbose) Serial.println("switch LOW"); 
		digitalWrite(LED, LOW);
		limitswitch = 0; 
		nextlimitswitch = time + 20000; // microseconds, debounce delay
		return;
	}
  
	if( (digitalRead(SWITCH) == HIGH) && (limitswitch == 0) ) { // on
		boolean h = homing;
		if (stepperenabled) disableMotor();
		up();
		if (verbose) Serial.println("switch HIGH"); 
		digitalWrite(LED, HIGH);
		limitswitch = 1; 
		nextlimitswitch = time + 20000;
		position = 0;
		if (h) { // homing mode, true now move to home
			go(HOMEPOSITION);
			homing = h;
		}
		return;
	}
	
	nextlimitswitch = time + 1000;
	
}

/* periodic output status to serial */
void printOutput() {
	
	/* INCOMPLETE OUTPUT! ( see printShortOutput() ) */
	
	Serial.print(F("load: "));
	Serial.print(currentload);
	Serial.print(F("  Z pos: "));
	Serial.print(position);
	
	Serial.print(F("  top heat: "));
	if (topheaterson) Serial.print(F("ON "));
	else Serial.print(F("OFF "));
	if(tempControlActiveTOP) Serial.print(F("AUTO "));
	Serial.print(toptemp);
	Serial.print(F("C  "));
	
	Serial.print(F("bottom heat: "));
	if (bottomheaterson) Serial.print(F("ON "));
	else Serial.print(F("OFF "));
	if(tempControlActiveBOTTOM) Serial.print(F("AUTO "));
	Serial.print(bottomtemp);
	Serial.print(F("C  "));

	Serial.print(F("case temp: "));
	Serial.print(internaltemp);
	Serial.print(F("C"));
	
	Serial.print(F("  setpointTOP: "));
	Serial.print(setPointTOP);
	Serial.print(F("C"));
	
	Serial.print(F("  setpointBOTTOM: "));
	Serial.print(setPointBOTTOM);
	Serial.print(F("C"));

	Serial.print(F("  ovenfan: "));
	if (convectfanon) Serial.print(F(" ON"));
	else Serial.print(F(" OFF"));

	Serial.print(F("  casefan: "));
	if (casefan) Serial.print(F(" ON"));
	else Serial.print(F(" OFF"));
	
	Serial.println("");
		
	nextOutput = time + OUTPUTINTERVAL;
}

void printShortOutput() {
	
	Serial.print("<");
	
	if(stepperenabled && !autoloadhold) Serial.print("moving"); // press status
	else if (autoloadhold) Serial.print("auto-up");
	else Serial.print("stopped");
	Serial.print(" ");
		
	Serial.print(targetLoad);    // force target kg

	Serial.print(" ");
	Serial.print(internaltemp);   // case temp deg C
	Serial.print(" ");  
	Serial.print(bottomtemp);  		// oven temp deg C
	Serial.print(" ");
	Serial.print(toptemp);  		// bar temp dec C

	Serial.print(" ");
	if (topheaterson) Serial.print("ON");  // top heaters ON/OFF
	else Serial.print("OFF");		

	Serial.print(" ");
	if (bottomheaterson) Serial.print("ON");   // bottom heaters ON/OFF
	else Serial.print("OFF");		

	Serial.print(" ");
	if (convectfanon) Serial.print("ON"); 		// convectfan   ON/OFF
	else Serial.print("OFF");	
	
	Serial.print(" ");
	Serial.print(setPointBOTTOM);  		// bottom temp set point degC

	Serial.print(" ");	
	if (tempControlActiveBOTTOM) Serial.print("YES");   // bottom temp control active YES/NO
	else Serial.print("NO");

	Serial.print(" ");
	Serial.print(setPointTOP);  		// top temp set point degC

	Serial.print(" ");	
	if (tempControlActiveTOP) Serial.print("YES");   // top temp control active YES/NO
	else Serial.print("NO");
	
	Serial.print(" ");
	if (casefanon) Serial.print("ON"); 		// convectfan   ON/OFF
	else Serial.print("OFF");
	
	Serial.print(" ");
	Serial.print(targetpos);   
	
	Serial.println(">");
	
	nextOutput = time + OUTPUTINTERVAL;

}

void printForceOutput() {
	if (verbose) return;
	
	Serial.print("<");
	Serial.print(position);    // 1 position
	Serial.print(" ");
	Serial.print(currentload);   // 2 current force kg
	Serial.println(">");
	
	nextForceOutput = time + FORCEOUTPUTINTERVAL;
}

/* read load cell */
void loadread() {

	if (!scale.is_ready()) return;

	unsigned long value = 0;
	uint8_t data[3] = { 0 };
	uint8_t filler = 0x00;

	data[2] = shiftIn(DOUT, CLK, MSBFIRST);
	data[1] = shiftIn(DOUT, CLK, MSBFIRST);
	data[0] = shiftIn(DOUT, CLK, MSBFIRST);

	digitalWrite(CLK, HIGH);
	digitalWrite(CLK, LOW);

	if (data[2] & 0x80) {
		filler = 0xFF;
	} else {
		filler = 0x00;
	}

	value = ( static_cast<unsigned long>(filler) << 24
			| static_cast<unsigned long>(data[2]) << 16
			| static_cast<unsigned long>(data[1]) << 8
			| static_cast<unsigned long>(data[0]) );
			
	currentload = (static_cast<long>(value)-scale.get_offset())/scale.get_scale();
		
	// check for interference with tray, either up or down
	if (stepperenabled && abs(currentload) >= SAFELOAD && position < SAFEPOSTHRESHOLD) {
		disableMotor();
		if (verbose) Serial.println("safezone interference detected");

	}
	// check for target load reached (up movement only, no restriction for down movement)
	else if (stepperenabled && currentload >= targetLoad && direction == DIRECTIONUP) {
		disableMotor();
		if (verbose) Serial.println("load limit reached");
	}
	
	if (!stepperenabled and autoloadhold and abs(currentload - targetLoad) > LOADTOLERANCE) {
		if (currentload > targetLoad) { down(); go(0.1); }
		else { up(); go(0.1); }
	}
	
		
	// fast or slow speed depending on force, position	
	if (position > SAFEPOSTHRESHOLD && currentload > SAFELOAD && direction == DIRECTIONUP) speeddelay = SPEEDDELAYSLOW;
	else if (direction == DIRECTIONUP && position == POSITIONUNKNOWN) speeddelay = SPEEDDELAYSLOW;
	else speeddelay = SPEEDDELAYNORMAL;
	
	nextLoadCheck = time + LOADCHECKINTERVAL;
	
}


void readTopTemp() {
	
	if (stepperenabled && time - nextTopTempCheck < MAXTEMPCHECKINTERVAL) {
		nextTopTempCheck = nextTopTempCheck + 0.5*SECOND;
		return;
	}

	nextTopTempCheck = time + TEMPCHECKINTERVAL;
		
	toptemp = thermocoupleTop.readCelsius();

	if (isnan(toptemp)) {
		toptempnan ++;
		nextTopTempCheck = time + 1*SECOND;
		if (toptempnan >= MAXTEMPNAN) {
			topheaters(TURNOFF);
			if (verbose) Serial.print(F("max nan reached"));
		}
	}
	else toptempnan = 0; 
	
	if (toptemp >= MAXTEMP) topheaters(TURNOFF);
	
	lastTempDiffTOP = toptemp - lastTempTOP;
	lastTempTOP = toptemp;

}


void readBottomTemp() {
	
	if (stepperenabled && time - nextBottomTempCheck < MAXTEMPCHECKINTERVAL) {
		nextBottomTempCheck = nextBottomTempCheck + 0.5*SECOND;
		return;
	}
	
	nextBottomTempCheck = time + TEMPCHECKINTERVAL;
	
	bottomtemp = thermocoupleBottom.readCelsius();

	if (isnan(bottomtemp)) {
		bottomtempnan ++;
		nextBottomTempCheck = time + 1*SECOND;
		if (bottomtempnan >= MAXTEMPNAN) {
			bottomheaters(TURNOFF);
			if (verbose) Serial.print(F("max nan reached"));
		}
	}
	else bottomtempnan = 0; 
	
	if (bottomtemp >= MAXTEMP) bottomheaters(TURNOFF);
	
	lastTempDiffBOTTOM = bottomtemp - lastTempBOTTOM;
	lastTempBOTTOM = bottomtemp;

}


void readInternalTemp(){
	
	if (stepperenabled && time - nextInternalTempCheck < MAXTEMPCHECKINTERVAL) {
		nextInternalTempCheck = nextBottomTempCheck + 2*SECOND;
		return;
	}
	
	internaltemp = thermocoupleBottom.readInternal();
	
	if (internaltemp > MAXINTERNALTEMP) {
		allOff();
		if (verbose) Serial.println("max internal temp reached");
	}
	
	if (internaltemp > CASEFANTHRESHOLD && !casefanon && !casefanstayoff) casefan(TURNON);
	if (internaltemp < CASEFANTHRESHOLD-2 && casefanon && !casefanstayoff) casefan(TURNOFF);

	nextInternalTempCheck = time + TEMPCHECKINTERVAL;
}

void tempControlStartTOP() {
	tempControlActiveTOP = true;
	windowStartTimeTOP = time;
	if(verbose) Serial.println("TOP temp control ON");
}

void tempControlStartBOTTOM() {
	tempControlActiveBOTTOM = true;
	windowStartTimeBOTTOM = time;
	if(verbose) Serial.println("BOTTOM temp control ON");
}

void topTempControlStop() {
	tempControlActiveTOP = false;
	topheaters(TURNOFF);
	if (verbose) Serial.println(F("top temp control OFF"));
}

void bottomTempControlStop() {
	tempControlActiveBOTTOM = false;
	bottomheaters(TURNOFF);
	if (verbose) Serial.println(F("bottom temp control OFF"));
}

void tempControlTop() {
	
	if (isnan(lastTempTOP)) return;
	
	if (lastTempTOP < setPointTOP - AUTOALWAYSONDIFF) {
		if (!topheaterson) topheaters(TURNON);
		return;
	}
		
	if (time - windowStartTimeTOP > DUTYWINDOW_TOP)   { //time to shift the Relay Window
		windowStartTimeTOP = time;
	}
	
	double target = setPointTOP;
	
	if (lastTempDiffTOP > 0.25) target -= 3;
	else if (lastTempDiffTOP <= 0) target += 10;

	double gap = target-lastTempTOP; // current temperature gap to setpoint
	double dutyCycle = 1.0;
	if (gap < 8) dutyCycle = 0; // completely off when almost there (includes overshoots)
	else if (gap < 15) dutyCycle = gap/15; // slow down when getting close

	double output = dutyCycle * DUTYWINDOW_TOP * DUTYPERCENT_TOP;

	if (output > time- windowStartTimeTOP && lastTempTOP < setPointTOP) {
		if (!topheaterson) topheaters(TURNON);
	}
	else {
		if (topheaterson) topheaters(TURNOFF);
	}
	
}

void tempControlBottom() {
	
	if (isnan(lastTempBOTTOM)) return;
		
	if (lastTempBOTTOM < setPointBOTTOM - AUTOALWAYSONDIFF) {
		if (!bottomheaterson) bottomheaters(TURNON);
		return;
	}
		
	if (time - windowStartTimeBOTTOM > DUTYWINDOW_BOTTOM)   { //time to shift the Relay Window
		windowStartTimeBOTTOM = time;
	}
	
	double target = setPointBOTTOM;
	
	if (lastTempDiffBOTTOM > 0.25) target -= 3;
	else if (lastTempDiffBOTTOM <= 0) target += 10;

	double gap = target-lastTempBOTTOM; // current temperature gap to setpoint
	double dutyCycle = 1.0;
	if (gap < 3) dutyCycle = 0; // completely off when almost there (includes overshoots)
	else if (gap < 15) dutyCycle = gap/15; // slow down when getting close

	double output = dutyCycle * DUTYWINDOW_BOTTOM * DUTYPERCENT_BOTTOM;

	if (output > time- windowStartTimeBOTTOM && lastTempBOTTOM < setPointBOTTOM) {
		if (!bottomheaterson) bottomheaters(TURNON);
	}
	else {
		if (bottomheaterson) bottomheaters(TURNOFF);
	}
	
}

void toggleVerbose() {
	if (verbose) {
		Serial.println("verbose off");
		verbose = false;
	}
	else {
		Serial.println("verbose on");
		verbose = true;
	}
}

void version() {
	Serial.println(F("<bartendr2B: version 0.3>"));
}
